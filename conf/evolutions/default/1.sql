# --- Users schema

# --- !Ups

CREATE TABLE users (
    id            SERIAL       NOT NULL PRIMARY KEY,
    email         VARCHAR(255) UNIQUE NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    created_at    TIMESTAMP    NOT NULL,
    updated_at    TIMESTAMP
);

# --- !Downs

DROP TABLE users IF EXISTS;
