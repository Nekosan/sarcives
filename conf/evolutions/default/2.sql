# --- Roles schema

# --- !Ups

CREATE TABLE admins (
  id      SERIAL         NOT NULL PRIMARY KEY,
  user_id INTEGER UNIQUE NOT NULL REFERENCES users (id) ON DELETE CASCADE
);

CREATE TABLE creators (
  id      SERIAL         NOT NULL PRIMARY KEY,
  user_id INTEGER UNIQUE NOT NULL REFERENCES users (id) ON DELETE CASCADE
);

# --- !Downs
DROP TABLE admins IF EXISTS;
DROP TABLE creators IF EXISTS;
