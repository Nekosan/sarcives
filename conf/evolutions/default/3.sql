# --- Roles schema

# --- !Ups

CREATE TABLE documents (
  id         serial primary key,
  creator_id integer null references creators (id) on delete set null,
  doc_type   varchar(80),
  caption    text,
  doc_date   timestamp,
  place      varchar(255),
  url        text
);

CREATE INDEX documents_creator_id_idx ON documents(creator_id);

# --- !Downs
DROP TABLE IF EXISTS documents;
