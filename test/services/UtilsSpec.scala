package services

import org.scalatestplus.play.PlaySpec
import services.Utils.basename

class UtilsSpec extends PlaySpec {
  "basename" should {
    "give the basename for a file path" in {
      basename("/home/user/bla.jpg") mustBe "bla.jpg"
      basename("C:\\home\\user\\bla.jpg") mustBe "bla.jpg"
    }
  }
}
