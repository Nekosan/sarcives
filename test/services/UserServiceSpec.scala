package services

import helpers.TestHelpers._
import models.{Admins, Creators, Login, Users}
import org.scalatestplus.play._
import play.api.test.WithApplication

import scala.slick.driver.PostgresDriver.simple._

class UserServiceSpec extends PlaySpec {
  "A UserService" should {
    val creatorLogin = Login("email", "pass", Some(true), Some(false))
    "create a user from a login" when {
       "no one has this email" in new WithApplication(fakeApp) {
         withTestDB { implicit session =>
           UserService.createUser(creatorLogin).isRight mustBe true
           TableQuery[Users].length.run mustBe 1
           TableQuery[Creators].length.run mustBe 1
           TableQuery[Admins].length.run mustBe 0
         }
       }
    }

    "not create a user from a login" when {
      "given an existing email" in new WithApplication(fakeApp) {
        withTestDB { implicit session =>
          UserService.createUser(creatorLogin)
          UserService.createUser(creatorLogin).isLeft mustBe true
        }
      }
    }

    "retrieve a user from a login" when {
      "the right login info are provided" in new WithApplication(fakeApp) {
        withTestDB { implicit session =>
          val userId = UserService.createUser(creatorLogin)
                        .fold({ _ => throw new Exception("No use found")}, identity)
          val user = TableQuery[Users].filter(_.id === userId).first
          UserService.findUserByLogin(creatorLogin) mustBe Some(user)
        }
      }
    }

    "not retrieve a uesr from a login" when {
      "the wrong login info are provided" in new WithApplication(fakeApp) {
        withTestDB { implicit session =>
          UserService.createUser(creatorLogin)
            .fold({ _ => throw new Exception("No use found")}, identity)

          val wrongLogin = Login("email", "wrong pass", Some(true), Some(false))
          UserService.findUserByLogin(wrongLogin) mustBe None
        }
      }
    }

    "retrive a user's role ids" in new WithApplication(fakeApp) {
      withTestDB { implicit session =>
        val userId = UserService.createUser(creatorLogin).right.get
        val user = TableQuery[Users].filter(_.id === userId).first
        val creatorId = TableQuery[Creators].
                          filter(_.userId === userId).
                          map(_.id).
                          firstOption

        UserService.roleIds(user) mustBe (creatorId, None)
      }
    }
  }
}
