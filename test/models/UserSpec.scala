package models

import org.scalatestplus.play.PlaySpec

class UserSpec extends PlaySpec {
  "A User" must {
    val login = Login("name", "password", None, None)
    val user = login.createUser

    "be authenticated against a right password" in {
      user.authenticate(login) mustBe true
    }

    "not be authenticated against a wrong password" in {
      user.authenticate(Login("name", "wrong", None, None)) mustBe false
    }
  }
}
