package presenters

import org.scalatestplus.play.PlaySpec

class CurrentUserSpec extends PlaySpec {
  "A CurrentUser" should {
    "be convertible to a list of session keys" in {
      LoggedInUser(1, "email", Some(1), None).toSessionMap mustBe Map(
        "currentUserId"        -> "1",
        "currentUserEmail"     -> "email",
        "currentUserCreatorId" -> "1"
      )

      AnonymousUser.toSessionMap mustBe Map.empty
    }

    "be creatable from a session map" in {
      CurrentUser.fromSessionMap(Map(
        "currentUserId"        -> "1",
        "currentUserEmail"     -> "email",
        "currentUserCreatorId" -> "bla"
      )) mustBe LoggedInUser(1, "email", None, None)

      CurrentUser.fromSessionMap(Map(
        "currentUserAdminId" -> "1"
      )) mustBe AnonymousUser
    }
  }
}
