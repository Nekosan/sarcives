package helpers

import play.api.db.DB
import play.api.test.FakeApplication

import scala.slick.driver.PostgresDriver.simple._

object TestHelpers {
  def fakeApp = new FakeApplication()

  def withTestDB(testFun: Session => Unit)(implicit app: FakeApplication) = {
    val ds = DB.getDataSource()
    val db = Database.forDataSource(ds)

    db withTransaction { implicit session =>
      testFun(session)
      session.rollback()
    }
  }
}
