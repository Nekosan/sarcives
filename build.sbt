import play.PlayScala

name := """sarcives"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "com.typesafe.slick" %% "slick" % "2.1.0-M2",
  "org.postgresql" % "postgresql" % "9.3-1101-jdbc41",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.scalatestplus" %% "play" % "1.1.0" % "test",
  "org.webjars" % "bootstrap" % "3.2.0",
  "org.webjars" % "angularjs" % "1.2.23"
)

lazy val root = (project in file(".")).enablePlugins(PlayScala)

fork in Test := false

includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"
