package services

import models._
import play.api.{Logger, Play}

import scala.collection.JavaConversions._
import scala.slick.driver.PostgresDriver.simple._

object UserService {
  lazy val users    = TableQuery[Users]
  lazy val creators = TableQuery[Creators]
  lazy val admins   = TableQuery[Admins]


  def findUserByLogin(login: Login)(implicit session: Session): Option[User] = {
    findByEmail(login.email).filter(_.authenticate(login))
  }

  def createUser(login: Login)(implicit session: Session): Either[String, Int] = {
    findByEmail(login.email) match {
      case Some(_) => Left("We have someone with the same email already. Please choose another one!")
      case None    => {
        val newUserId = users returning users.map(_.id) += login.createUser
        if (login.isCreator == Some(true)) creators += Creator(None, newUserId)
        if (login.isAdmin == Some(true))   admins   += Admin(None, newUserId)
        Right(newUserId)
      }
    }
  }

  def emailNotTaken(email: String)(implicit session: Session): Boolean = findByEmail(email).isEmpty

  def seedAdmins(implicit session: Session) = {
    val admins = Play.current.configuration.getObject("sarcives.admins")
    admins.foreach { confObject =>
      confObject.entrySet.foreach { obj =>
        val email    = obj.getKey
        val password = obj.getValue.unwrapped.asInstanceOf[String]
        val login    = Login(email, password, Some(false), Some(true))
        createUser(login) match {
          case Right(_) => Logger.info(s"Admin user $email created")
          case Left(_)  => Logger.info(s"Admin user $email already created")
        }
      }
    }
  }

  def roleIds(user: User)(implicit session: Session): (Option[Int], Option[Int]) = {
    val usersQuery = users.filter(_.email === user.email)

    val creatorId = usersQuery
                      .leftJoin(creators)
                      .on(_.id === _.userId)
                      .map(_._2.id.?)
                      .firstOption.flatten

    val adminId = usersQuery
                    .leftJoin(admins)
                    .on(_.id === _.userId)
                    .map(_._2.id.?)
                    .firstOption.flatten

    (creatorId, adminId)

  }

  private def findByEmail(email: String)(implicit session: Session): Option[User] = {
    users.filter(_.email === email).firstOption
  }

}
