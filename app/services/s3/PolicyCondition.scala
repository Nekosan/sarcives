package services.s3

import play.api.libs.json._

abstract sealed class PolicyCondition {
  def toJson: JsValue
}

case class EqualityCondition(key: String, value: String) extends PolicyCondition {
  override def toJson = Json.obj(key -> value)
}

case class StartsWithCondition(key: String, value: String) extends PolicyCondition {
  override def toJson = Json.arr("starts-with", "$" + key, value)
}

case class ContentLengthRange(min: Int, max: Int) extends PolicyCondition {
  override def toJson = Json.arr("content-length-range", min, max)
}
