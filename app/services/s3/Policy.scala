package services.s3

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}

import play.api.libs.json._

case class Policy(expiration: Date, conditions: List[PolicyCondition]) {
  def formatDate(date: Date) = {
    val fmt = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss'Z'")
    fmt.setTimeZone(TimeZone.getTimeZone("GMT"))
    fmt.format(date)
  }

  def toJson: JsValue = {
    Json.obj(
      "expiration" -> formatDate(expiration),
      "conditions" -> conditions.map(_.toJson)
    )
  }
}
