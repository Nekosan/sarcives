package services.s3

import play.api.libs.json._

case class UploadParams(
    url:            String,
    key:            String,
    acl:            String,
    awsAccessKeyId: String,
    policy:         String,
    signature:      String,
    contentType:    String
  ) {
  def toJson: JsValue = {
    Json.obj(
      "url"            -> url,
      "key"            -> key,
      "acl"            -> acl,
      "awsAccessKeyId" -> awsAccessKeyId,
      "policy"         -> policy,
      "signature"      -> signature,
      "contentType"    -> contentType
    )
  }
}
