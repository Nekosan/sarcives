package services.s3

import java.util.Date
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

import play.api.Play
import play.api.libs.MimeTypes
import play.api.libs.json._
import services.Utils.basename
import sun.misc.BASE64Encoder

object ImageService {
  private val defaultExpirationSec = 10
  private val defaultMaxSize = 10 * 1024 * 1024

  private val conf = Play.current.configuration
  private val expirationTimeMillis =
    conf.getInt("aws.expiration-sec").getOrElse(defaultExpirationSec) * 1000

  // Would have been great to use Typesafe's config built-in size parsing
  // capability, if only it was documented...
  private val maxSize = conf.getInt("aws.max-doc-size").getOrElse(defaultMaxSize)

  private def getPropOrCrash(key: String): String = {
    val prop = conf.getString(key)
    prop.getOrElse(throw new Exception(s"Need a $key property"))
  }

  private val bucketName  = getPropOrCrash("aws.bucket")
  private val accessKeyId = getPropOrCrash("aws.access-key-id")
  private val secretKey   = getPropOrCrash("aws.secret-key")

  def s3Params(filePath: String): UploadParams = {
    val fileName    = basename(filePath)
    val url         = s"https://$bucketName.s3.amazonaws.com/"
    val key         = System.nanoTime.toString + "-" + fileName
    val acl         = "public-read"
    val contentType = MimeTypes.forFileName(fileName).getOrElse("application/octet-stream")
    val expiration  = new Date(System.currentTimeMillis() + expirationTimeMillis)

    val conditions = List(
      EqualityCondition("acl", acl),
      EqualityCondition("bucket", bucketName),
      EqualityCondition("key", key),
      StartsWithCondition("Content-Type", ""),
      ContentLengthRange(0, maxSize)
    )

    val policy        = Policy(expiration, conditions)
    val policyBytes   = Json.stringify(policy.toJson).getBytes("UTF-8")
    val encodedPolicy = (new BASE64Encoder).encode(policyBytes).replace("\n", "").replace("\r", "")

    val hmac = Mac.getInstance("HmacSHA1")
    hmac.init(new SecretKeySpec(secretKey.getBytes("UTF-8"), "HmacSHA1"))
    val signature = (new BASE64Encoder).encode(hmac.doFinal(encodedPolicy.getBytes("UTF-8"))).replace("\n", "")

    UploadParams(url, key, acl, accessKeyId, encodedPolicy, signature, contentType)
  }
}
