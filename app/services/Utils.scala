package services

object Utils {
  def basename(filePath: String): String = {
    filePath.reverse.takeWhile {c => c != '/' && c != '\\'}.reverse
  }
}
