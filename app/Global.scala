import play.api.db.DB
import play.api.Play.current
import play.api.{Mode, Play, Application, GlobalSettings}
import services.UserService
import scala.slick.driver.PostgresDriver.simple._

object Global extends GlobalSettings{
  override def onStart(app: Application) {
    val ds = DB.getDataSource()
    val db = Database.forDataSource(ds)

    db withTransaction { implicit session =>
      if (Play.current.mode != Mode.Test) {
        UserService.seedAdmins
      }
    }
  }
}
