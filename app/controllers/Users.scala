package controllers

import models.Login
import play.api.data.Form
import play.api.mvc.Controller
import services.UserService
import AdminAction.adminAction

import play.api.db._
import play.api.Play.current
import scala.slick.driver.PostgresDriver.simple._
import CurrentUserProvider.currentUser

object Users extends Controller {
  def newUser = adminAction { implicit userReq =>
    implicit val req = userReq.request

    db withSession { implicit session =>
      Ok(views.html.users.create(Login.userForm))
    }
  }

  def create = adminAction { implicit userReq =>
    implicit val req = userReq.request

    db withTransaction { implicit session =>
      def success(login: Login) = {
        UserService.createUser(login)
        Redirect(routes.Application.index())
      }
      def error(form: Form[Login]) = BadRequest(views.html.users.create(form))

      Login.userForm.bindFromRequest.fold(error, success)
    }
  }

  private def ds = DB.getDataSource()
  private def db = Database.forDataSource(ds)
}
