package controllers
import play.api.mvc._
import Results.Forbidden
import scala.concurrent._

object AdminAction extends ActionFilter[UserRequest] {
  def filter[A](input: UserRequest[A]) = Future.successful {
    if (input.currentUser.isAdmin) {
      None
    } else {
      Some(Forbidden)
    }
  }

  def adminAction = UserAction andThen AdminAction
}
