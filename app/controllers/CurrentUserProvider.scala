package controllers

import play.api.mvc.Request
import presenters.CurrentUser

object CurrentUserProvider {
  implicit def currentUser[A](implicit request: Request[A]): CurrentUser = {
    CurrentUser.fromSessionMap(request.session.data)
  }
}
