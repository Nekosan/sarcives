package controllers
import play.api.mvc._
import Results.Forbidden
import scala.concurrent._

object CreatorAction extends ActionFilter[UserRequest] {
  def filter[A](input: UserRequest[A]) = Future.successful {
    if (input.currentUser.isCreator) {
      None
    } else {
      Some(Forbidden)
    }
  }

  def creatorAction = UserAction andThen CreatorAction
}
