package controllers

import presenters.CurrentUser
import play.api.mvc._
import CurrentUserProvider.currentUser

import scala.concurrent.Future
import scala.language.implicitConversions

case class UserRequest[A](currentUser: CurrentUser, request: Request[A])

object UserRequest {
  implicit def req2user[A](req: UserRequest[A]): CurrentUser = req.currentUser
}

object UserAction extends ActionBuilder[UserRequest] with ActionTransformer[Request, UserRequest]{
  def transform[A](request: Request[A]) = Future.successful {
    UserRequest[A](currentUser(request), request)
  }
}
