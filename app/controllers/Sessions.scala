package controllers

import models.{User, Login}
import play.api.data.Form
import presenters.LoggedInUser
import services.UserService
import CurrentUserProvider.currentUser

import play.api.db._
import play.api.mvc._
import play.api.Play.current
import scala.slick.driver.PostgresDriver.simple._

object Sessions extends Controller {
  def newSession = Action { implicit request =>
    db withSession { implicit session =>
      Ok(views.html.sessions.create(Login.loginForm))
    }
  }

  def create = Action { implicit request =>
    db withSession { implicit session =>
      def success(login: Login) = {
        UserService.findUserByLogin(login) match {
          case Some(u@User(Some(id), email, _, _, _)) => {
            val (creatorId, adminId) = UserService.roleIds(u)
            val currentUser = LoggedInUser(id, email, creatorId, adminId)
            Redirect(routes.Application.index()).withSession(Session(currentUser.toSessionMap))
          }
          case _ => {
            val formWithError = Login.loginForm.
                                  fill(Login(login.email, "", None, None)).
                                  withGlobalError("Authentication failed")
            BadRequest(views.html.sessions.create(formWithError))
          }
        }
      }
      def error(form: Form[Login]) = BadRequest(views.html.sessions.create(form))

      Login.loginForm.bindFromRequest.fold(error, success)
    }
  }

  def destroy = Action { implicit request =>
    Redirect(routes.Sessions.newSession()).withNewSession
  }

  private def ds = DB.getDataSource()

  private def db = Database.forDataSource(ds)
}
