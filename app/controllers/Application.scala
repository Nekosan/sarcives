package controllers

import play.api.mvc._
import CurrentUserProvider.currentUser

object Application extends Controller {
  def index = Action { implicit request =>
    Ok(views.html.index())
  }
}
