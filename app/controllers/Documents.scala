package controllers

import controllers.CreatorAction.creatorAction
import models.{Document, Documents => DocumentsTable}
import play.api.Play.current
import play.api.db.DB
import play.api.libs.json._
import play.api.mvc.Controller
import services.s3.ImageService
import CurrentUserProvider.currentUser

import scala.slick.driver.PostgresDriver.simple._

object Documents extends Controller {
  private def ds = DB.getDataSource()
  private def db = Database.forDataSource(ds)

  def newDocument = (UserAction andThen CreatorAction) { implicit userReq =>
    implicit val req = userReq.request
    Ok(views.html.documents.newDocument())
  }

  def uploadParams = creatorAction { userRequest =>
    val oJson = userRequest.request.body.asJson

    val fileName = for {
      json <- oJson
      name <- (json \ "filePath").validate[String].asOpt
    } yield name

    fileName.fold(BadRequest("filePath parameter missing.")) { fn: String =>
      val uploadParams = ImageService.s3Params(fn)
      Ok(Json.toJson(uploadParams.toJson))
    }
  }

  def create = creatorAction { implicit userRequest =>
    implicit val req = userRequest.request
    val form =  Document.form.bindFromRequest

    form.fold(
      errors => BadRequest(errors.errorsAsJson),
      doc    => {
        db withTransaction { implicit s =>
          TableQuery[DocumentsTable].insert(doc)
        }
        Created.flashing("success" -> "Document created!")
      }
    )
  }

}
