package models

import scala.slick.driver.PostgresDriver.simple._

case class Admin(id: Option[Int], userId: Int)

case class Creator(id: Option[Int], userId: Int)

class Creators(tag: Tag) extends Table[Creator](tag, "creators") {
  def id     = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Int]("user_id")
  def user   = foreignKey("creators_user_id_fkey", userId, TableQuery[Users])(_.id)

  def * = (id.?, userId) <> (Creator.tupled, Creator.unapply)
}

class Admins(tag: Tag) extends Table[Admin](tag, "admins") {
  def id     = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Int]("user_id")
  def user   = foreignKey("admins_user_id_fkey", userId, TableQuery[Users])(_.id)

  def * = (id.?, userId) <> (Admin.tupled, Admin.unapply)
}
