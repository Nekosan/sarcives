package models

import java.sql.Timestamp

import org.mindrot.jbcrypt.BCrypt

import scala.slick.driver.PostgresDriver.simple._

case class User(id: Option[Int], email: String, passwordHash: String, createdAt: Option[Timestamp], updatedAt: Option[Timestamp]) {
  def authenticate(login: Login): Boolean = BCrypt.checkpw(login.clearPassword, passwordHash)
}

class Users(tag: Tag) extends Table[User](tag, "users") {
  def id           = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def email        = column[String]("email")
  def passwordHash = column[String]("password_hash")
  def createdAt    = column[Timestamp]("created_at")
  def updatedAt    = column[Timestamp]("updated_at")

  def * = (id.?, email, passwordHash, createdAt.?, updatedAt.?) <> (User.tupled, User.unapply)
}
