package models

import java.sql.Timestamp
import java.util.Date

import org.mindrot.jbcrypt.BCrypt
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.Messages
import services.UserService.emailNotTaken

import scala.slick.driver.PostgresDriver.simple._

case class Login(
    email: String,
    clearPassword: String,
    isCreator: Option[Boolean],
    isAdmin: Option[Boolean]) {
  def createUser: User = {
    val hashedPassword = BCrypt.hashpw(clearPassword, BCrypt.gensalt())
    val date = new Date()
    val timestamp = new Timestamp(date.getTime)
    User(None, email, hashedPassword, Some(timestamp), None)
  }
}

object Login {
  def loginForm(implicit session: Session) = {
    def formToFields(login: Login) = Some(login.email, login.clearPassword)
    Form(
      mapping(
        "email"    -> email,
        "password" -> text
      )(Login.apply(_, _, None, None))(formToFields)
    )
  }

  def userForm(implicit session: Session) = Form(
    mapping(
      "email"     -> email.verifying(Messages("user.email-taken"), emailNotTaken _),
      "password"  -> text(minLength = 6),
      "isCreator" -> optional(boolean),
      "isAdmin"   -> optional(boolean)
    )(Login.apply)(Login.unapply)
  )
}
