package models

import java.net.URL
import java.sql.Timestamp
import java.text.SimpleDateFormat

import controllers.UserRequest
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation._
import presenters.{AnonymousUser, LoggedInUser}

import scala.slick.driver.PostgresDriver.simple._
import scala.util.{Failure, Success, Try}

object DocumentType {
  def read(s: String): Option[DocumentType] = {
    List(Photo, Letter, OfficialDoc, Article).find(_.toString == s)
  }
}

sealed trait DocumentType

case object Photo       extends DocumentType
case object Letter      extends DocumentType
case object OfficialDoc extends DocumentType
case object Article     extends DocumentType

case class Document(
    id:        Option[Int],
    creatorId: Option[Int],
    docType:   DocumentType,
    caption:   String,
    docDate:   Timestamp,
    place:     String,
    url:       URL
  ) {
  require(url.toURI.isAbsolute, "The URL needs to be absolute")
}

object Document {
  private val validUrl = Constraint[String] { str: String =>
    Try { new URL(str).toURI } match {
      case Failure(_) => Invalid(s"Invalid URL: $str")
      case Success(_) => Valid
    }
  }

  private val validDocType = Constraint[String] { str: String =>
    DocumentType.read(str) match {
      case None => Invalid(s"Invalid document type: $str. Valid ones are Photo, Letter, OfficialDoc and Article")
      case Some(_) => Valid
    }
  }

  private val sdf = new SimpleDateFormat("yyyy-MM-dd")

  private val validDate = Constraint[String] { str: String =>
    Try { sdf.parse(str) } match {
      case Failure(_) => Invalid("Invalid date format. Should be yyyy-MM-dd")
      case Success(_) => Valid
    }
  }

  def form(implicit req: UserRequest[_]) = {
    val creatorId = req.currentUser match {
      case AnonymousUser              => None
      case LoggedInUser(_, _, cId, _) => cId
    }

    def deserialize(creatorId: Option[Int])(
        id: Option[Int], docTypeStr: String, caption: String, date: String, place: String, urlStr: String
      ): Document = {
      val docType = DocumentType.read(docTypeStr).get
      val url = new URL(urlStr)
      val timestamp = new Timestamp(sdf.parse(date).getTime)
      Document(None, creatorId, docType, caption, timestamp, place, url)
    }

    def serialize(doc: Document) = doc match {
      case Document(id, _, docType, caption, docDate, place, url) =>
        Some(id, docType.toString, caption, sdf.format(docDate), place, url.toExternalForm)
    }

    Form(
      mapping(
        "id"      -> optional(number),
        "docType" -> nonEmptyText.verifying(validDocType),
        "caption" -> nonEmptyText,
        "date"    -> nonEmptyText.verifying(validDate),
        "place"   -> nonEmptyText(maxLength = 254),
        "url"     -> nonEmptyText.verifying(validUrl)
      )(deserialize(creatorId))(serialize)
    )
  }
}

class Documents(tag: Tag) extends Table[Document](tag: Tag, "documents") {
  type TupledDoc = (Option[Int], Option[Int], String, String, Timestamp, String, String)

  def id        = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def creatorId = column[Int]("creator_id")
  def docType   = column[String]("doc_type")
  def caption   = column[String]("caption")
  def docDate   = column[Timestamp]("doc_date")
  def place     = column[String]("place")
  def url       = column[String]("url")

  private def deserialize(tuple: TupledDoc): Document = {
    val (id, creatorId, docTypeStr, caption, docDate, place, urlStr) = tuple
    val docType = DocumentType.read(docTypeStr).getOrElse {
      throw new RuntimeException(s"Document type unknown: $docTypeStr")
    }
    val url = new URL(urlStr)

    Document(id, creatorId, docType, caption, docDate, place, url)
  }

  private def serialize(doc: Document): Option[TupledDoc] = doc match {
    case Document(id, creatorId, docType, caption, docDate, place, url) =>
      Some(id, creatorId, docType.toString, caption, docDate, place, url.toString)
  }

  def * = (id.?, creatorId.?, docType, caption, docDate, place, url) <> (deserialize, serialize)
}
