angular.module('sarServices', [])
  .factory('uploadService', ['$http', '$q', '$window',  ($http, $q, $window) ->
    service = {}

    service.uploadToS3 = (uploadParams) ->
      formData = new FormData()
      file     = document.querySelector("input[type='file']").files[0]
      url      = uploadParams.url

      formData.append('key',            uploadParams.key)
      formData.append('AWSAccessKeyId', uploadParams.awsAccessKeyId)
      formData.append('acl',            uploadParams.acl)
      formData.append('policy',         uploadParams.policy)
      formData.append('signature',      uploadParams.signature)
      formData.append('Content-Type',   uploadParams.contentType)
      formData.append('file',           file)

      $http.post(url, formData, {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined }
      }).then(
        (response) -> url + encodeURIComponent(uploadParams.key),
        (response) -> $q.reject(response.data.error_message)
      )

    service.getUploadParams = (filePath) ->
      return $q.reject("File not supplied") unless filePath

      $http.post("/documents/upload-params", { filePath: filePath }, {
        headers: { 'Content-Type': 'application/json' },
      }).then(
        (response) -> response.data,
        (response) -> $q.reject(response.data.error_message)
      )

    service.createDocument = (doc) ->
      $http.post("/documents", doc, {
        headers: { 'Content-Type': 'application/json' },
      }).then(
        (response) -> $window.location.assign('/'),
        (response) -> $q.reject(response.data.error_message)
      )

    service
  ])
