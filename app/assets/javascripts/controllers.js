angular.module('sarControllers', [])
  .controller('UploadController', ['$scope', 'uploadService', function($scope, uploadService) {
    $scope.document = { docType: "Photo"};

    $scope.uploadDocument = function() {
      var filePath = document.querySelector("input[type='file']").value;
      uploadService.getUploadParams(filePath).
      then(uploadService.uploadToS3).
      then(function(url) {
        $scope.document.url = url;
        uploadService.createDocument($scope.document);
      }).
      catch(function(err) { console.error(err); });
    };
  }]);
