package presenters

sealed trait CurrentUser {
  def isLoggedIn:   Boolean
  def isCreator:    Boolean
  def isAdmin:      Boolean
  def toSessionMap: Map[String, String]
}

object AnonymousUser extends CurrentUser {
  def isLoggedIn = false
  def isCreator  = false
  def isAdmin    = false
  def toSessionMap = Map.empty
}

case class LoggedInUser (
    id:        Int,
    email:     String,
    creatorId: Option[Int],
    adminId:   Option[Int]
  ) extends CurrentUser {
  def toSessionMap: Map[String, String] = {
    def toMap(roleId: Option[Int], key: String): Map[String, String] =
      roleId.fold(Map.empty[String, String]) { v: Int => Map(key -> v.toString) }

    Map("currentUserId" -> id.toString, "currentUserEmail" -> email) ++
    toMap(creatorId, "currentUserCreatorId") ++
    toMap(adminId, "currentUserAdminId")
  }

  def isLoggedIn = true
  def isCreator  = creatorId.isDefined
  def isAdmin    = adminId.isDefined

}

object CurrentUser {
  def fromSessionMap(sessionMap: Map[String, String]): CurrentUser = {
    def toInt(key: String):Option[Int] = {
      try {
        sessionMap.get(key).map(_.toInt)
      } catch {
        case e: NumberFormatException => None
        case e: Throwable             => throw e
      }
    }

    (toInt("currentUserId"), sessionMap.get("currentUserEmail")) match {
      case (Some(id), Some(email)) =>
        LoggedInUser(
          id,
          email,
          toInt("currentUserCreatorId"),
          toInt("currentUserAdminId")
        )
      case _ =>
        AnonymousUser
    }
  }
}
